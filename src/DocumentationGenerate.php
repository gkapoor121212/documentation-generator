<?php

namespace Drupal\security_review;

use Drupal\Core\State\StateInterface;

/**
 * A class containing static methods regarding the module's configuration.
 */
class DocumentationGenerate {
  /**
   * The state storage.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * Constructs a DocumentationGenerator instance.
   *
   * @param \Drupal\Core\State\StateInterface $state
   *   The state storage.
   */
  public function __construct(StateInterface $state) {
    // Store the dependencies.
    $this->state = $state;
  }
  
  /**
   * Returns the last time Documentation Generator has been run.
   *
   * @return int
   *   The last time Documentation Generator has been run.
   */
  public function getLastGenerate() {
    return $this->state->get('last_generate', 0);
  }

  /**
   * Sets the 'last_generate' value.
   *
   * @param int $last_generate
   *   The new value for 'last_generate'.
   */
  public function setLastGenerate($last_generate) {
    $this->state->set('last_generate', $last_generate);
  }

}
