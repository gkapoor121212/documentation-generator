<?php

namespace Drupal\documentation_generator\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\documentation_generator\DocumentationGenerate;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The class of the 'Documentation Generate' page's controller.
 */
class DocumentationGenerateController extends ControllerBase {
  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The documentation_generate service.
   *
   * @var \Drupal\documentation_generator\DocumentationGenerate
   */
  protected $documentationGenerate;

  /**
   * Constructs a DocumentationGenerateController.
   *
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   * @param \Drupal\documentation_generator\DocumentationGenerate $documentation_generate
   *   The documentation_generate service.
   */
  public function __construct(MessengerInterface $messenger, DocumentationGenerate $documentation_generate) {
    $this->messenger = $messenger;
    $this->documentationGenerate = $documentation_generate;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('messenger'),
      $container->get('documentation_generate')
    );
  }
  
  /**
   * Creates the Documentation Generate page.
   *
   * @return array
   *   The 'Run & Review' page's render array.
   */
  public function index() {
  	$generate_form = $this->formBuilder()->getForm('Drupal\documentation_generator\Form\DocumentationGenerateForm');

  	// Close the Run form if there are results.
  	if ($this->documentationGenerate->getLastGenerate() > 0) {
  		$generate_form['generate_form']['#open'] = FALSE;
  	}
  
    return [$generate_form, $this->results()];
  }

}
